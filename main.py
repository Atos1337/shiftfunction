import os

from scipy.stats.mstats import hdquantiles, mquantiles
from scipy.stats import norm, beta, dweibull
import numpy as np
import matplotlib.pyplot as plt


def shift_function(x, y, prob):
    return hdquantiles(x, prob) - hdquantiles(y, prob)


def var_quantile_estimate(x, prob):
    res = hdquantiles(x, prob, var=True)
    return res[1]


def real_shift_function(p, p_args, q, q_args, prob):
    return p.ppf(prob, *p_args) - q.ppf(prob, *q_args)


def experiment(p, p_args, q, q_args, sample_size, p_name, q_name):
    x = p.rvs(*p_args, size=sample_size)
    y = q.rvs(*q_args, size=sample_size)
    prob = np.arange(1, 100) / 100
    shift = shift_function(x, y, prob)
    real_shift = real_shift_function(p, p_args, q, q_args, prob)
    x_var = var_quantile_estimate(x, prob)
    y_var = var_quantile_estimate(y, prob)
    upper_shift = shift + x_var + y_var
    lower_shift = shift - x_var - y_var

    plt.clf()
    filepath = f'pics/{p_name}_{q_name}_{sample_size}.png'
    plt.plot(prob, shift, label='estimated_shift_function', color='green')
    plt.plot(prob, real_shift, label='real_shift_function', color='red')
    plt.fill_between(prob, lower_shift, upper_shift, color='green', alpha=.1)
    plt.legend()
    plt.savefig(filepath)


def main():
    experiment(norm, (), beta, (2, 8), 100, 'norm', 'beta')
    experiment(norm, (), beta, (2, 8), 10, 'norm', 'beta')
    experiment(norm, (), beta, (2, 8), 100000, 'norm', 'beta')
    experiment(norm, (), dweibull, (2,), 10, 'norm', 'dweibull')
    experiment(norm, (), dweibull, (2,), 100, 'norm', 'dweibull')
    experiment(norm, (), dweibull, (2,), 100000, 'norm', 'dweibull')
    experiment(dweibull, (2,), beta, (2, 8), 100, 'dweibull', 'beta')
    experiment(dweibull, (2,), beta, (2, 8), 10, 'dweibull', 'beta')
    experiment(dweibull, (2,), beta, (2, 8), 100000, 'dweibull', 'beta')


if __name__ == "__main__":
    main()
